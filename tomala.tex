\renewcommand{\arraystretch}{1.1}
% \renewcommand{\paragraph}[1]{\vskip1ex\noindent\textbf{#1}\enspace\ignorespaces}
\newcommand\dico{\textup{\textsf{di-co}}}
\newcommand\dispan{\textup{\textsf{di-span}}}
% \newcommand\Id{\textup{\textsf{Id}}}
\EIGchapter{Tristan}{Tomala}{Cheap-Talk and Persuasion Games}
{Economics and Decision Sciences Department, HEC Paris, France
\\
tomala@hec.fr}
%\auth{Tristan Tomala}

\begin{abstract}
This chapter studies sender-receiver games where an informed
player sends a message to an uninformed player who takes an
action.
We first study one-shot cheap talk games and
characterize the equilibrium outcomes.
Then, we consider different dynamic settings:
the long cheap talk case where the sender can send messages
repeatedly about the same state, and the repeated cheap talk
games, where the sequence of states follows an i.i.d.\ (or
Markovian) process.
\end{abstract}

\section{Sender-Receiver Problem}
% \subsection{Model}
A \textit{Sender-Receiver} problem models a two-player situation
where one agent possesses information and the other one is
able to take action.
The informed player, called \textit{sender}, can be thought of as an
expert who has superior information about a parameter called
the \textit{state}.
The uninformed player, called \textit{receiver}, has no information
but can choose a relevant action.
Payoffs for both players depend on the state and on the action.
The interaction between the two players is through
communication between the sender and the receiver.
Strategic conflicts arise when the preferences of both
players are not identical.

\noindent
A Sender-Receiver problem is given by the following data and timeline:
% \begin{itemize}
\bullitem The finite set of states is $\Theta$ endowed with a prior probability $p\in\Delta(\Theta)$.
\bullitem Player~1 knows the state and chooses a message $m$ in some  finite set $M$.
\bullitem Player~2 does not know the state and observes the message $m$. Then he chooses an action $a\in A$.
\bullitem Player 1's payoff is given by $u:\Theta\times A\to \reals$, player 2's payoff is given by $v:\Theta\times A\to \reals$.
%\end{itemize}

This defines the {\em cheap talk} game:
Player~1 sends a costless message to player~2 from some arbitrary set of messages.
A behavior strategy for player 1 in this game is
$\sigma:\Theta\to\Delta(M)$, and a behavior strategy for
player 2 is $\tau:M\to\Delta(A)$.

\begin{example}
\label{ex1}
Consider the Sender-Receiver game defined by the following table.
\[
\begin{array}{ccccc}
& a_1 & a_2&\\
\cline{1-3}
\theta_1&(1,5)&(0,1)&&\frac12\\
\cline{1-3}
 & & & \\
\cline{1-3}
\theta_2&(1,-10)&(0,1)&&\frac12\\
\cline{1-3}
\end{array}
\]
\end{example}

From the perspective of player 2, action $a_2$ is a safe bet
and $a_1$ is a risky bet.
Player 1 only cares about player~2 taking action $a_1$.
It is easy to find the equilibria on this game.
For a pair of strategies $(\sigma,\tau)$, denote by
$\sigma(m\mid\theta)$ the probability of sending message $m$
in state $\theta$, and by $\tau(a\mid m)$ the probability of
choosing action $a$ after receiving message $m$.

The game in Example~\ref{ex1} admits an equilibrium, called
\textit{babbling equilibrium}, 
where both strategies are constant mappings.
Because $\sigma$ is constant,
$\sigma(m\mid\theta)=\sigma(m\mid\theta')$ for all
$(\theta,\theta')$, this means that no information is
transmitted.
Therefore, for each message $m$, player~2 chooses an action 
\[
a^*\in\arg\max_{a\in A}\sum_\theta p(\theta)v(\theta,a).
\]
Due to the uniform prior, player 2 chooses the safe bet
$a_2$ for all messages, which is the best reply to player~1
sending no information.
Conversely, if player 2 plays the same action for all
messages, then player 1 is indifferent between all messages,
so we have an equilibrium.

This is the only equilibrium of the game.
To see this, suppose that for two messages $m,m'$ we have
$\tau(a_1\mid m)>\tau(a_1\mid m')$.
This implies that in equilibrium, player 1 does not choose
$m'$, because his payoff improves by choosing $m$ instead.
Therefore in equilibrium, player 2 chooses the same (mixed)
action for all messages, which leads back to the babbling
equilibrium.

% \subsection{Crawford and Sobel}
\section{Crawford and Sobel}

A prominent and seminal example is due to \citet{CrawfordSobel1982}.
In this model, sets are intervals of the real line. We
extend the definitions of strategies by considering mixed
messages or actions to be given by Borel probability
measures.
%\begin{itemize}
\bullitem The set of states is $\Theta=[0,1]$ with the Lebesgue measure.
\bullitem The set of messages is $M=[0,1]$. The set of actions is $A=\reals$.
\bullitem The payoff to player 1 is given, for some $b>0$, by
$u(\theta,a)=-(a-\theta-b)^2$.
\bullitem The payoff to player 2 is $v(\theta,a)=-(a-\theta)^2$.

\noindent
Ideally, player 2 wants to know the state and choose $a=\theta$. Player 1 has a bias $b>0$ and prefers player 2 to choose $\theta+b$.

By the same reasoning as in Example~\ref{ex1}, there is a
non-informative (babbling) equilibrium where
$\sigma(m\mid\theta)$ does not depend on $\theta$ and
$\tau(a\mid m)$ does not depend on $m$.
The action taken is $a^*=\frac12$,
the expected state under the uniform prior.

\paragraph{Partitional equilibrium.} We will construct informative equilibria.
Let $n\in\nats$ be an integer. Consider the partition of $[0,1]$ defined by:
\[
0=t_0<t_1<\cdots<t_{n-1}<t_n=1.
\]
Consider the following strategy of player 1,
\[
\sigma(\theta)=
\begin{cases}
m_k \text{ if } \theta\in[t_{k-1},t_k)\\
m_n \text{ if } \theta\in[t_{n-1},1]\\
\end{cases}
\]
where $m_1<\cdots<m_n$ are $n$ different messages.

A best reply of player 2 is to play $a_k=\tau(m_k)=\mathbb{E}(\theta\mid m_k)=\frac{t_{k-1}+t_k}{2}$ after message $m_k$, and the action 0 for messages $m\notin\{m_1,\dots,m_n\}$.

Fixing this strategy of player 2, let us find conditions
under which $\sigma$ a best reply for player 1.
Observe that player 1 should only send messages from
$\{m_1,\dots,m_n\}$; otherwise, this will bring the action
$0$ which is far from player 1's preferred action, due to the
bias.

%\begin{itemize}
\bullitem
If $\theta\in[t_{k-1},t_k)$, then player 1 should send $m_k$
but may want to deviate to $m_{k+1}$.
%\end{itemize}
An equilibrium condition is $u(t_k,a_k)=u(t_k,a_{k+1})$;
if $\theta=t_k$, then player 1 is indifferent. That is,
\[
(t_k-\frac{t_{k-1}+t_k}{2}-b)^2=(t_k-\frac{t_{k}+t_{k+1}}{2}-b)^2.
\]
Equivalently,
\[
t_k+b=\frac{\frac{t_{k-1}+t_k}{2}+\frac{t_{k}+t_{k+1}}{2}}{2}=\frac{t_{k-1}+2t_k+t_{k+1}}{4}\,.
\]
By induction, $t_k=kt_1+\frac{k(k-1)}{2}4b$.
Hence, $1=t_n=nt_1+n(n-1)2b$, that is,
$t_1=\frac1n-2(n-1)b$, and $t_k=\frac kn-2k(n-k)b$.

\begin{proposition}[\citealp{CrawfordSobel1982}]
There exists an $n$-partitional equilibrium for any
$n\le N(b)$, with $N(b)$ the largest $n$ such that $b<\frac{1}{2n(n-1)}$.
\end{proposition}
We can make the following comments on this result.
%\begin{itemize}
\bullitem All equilibria of the game induce the same joint distributions of states and actions as partitional equilibria.
\bullitem $N(b)\to\infty$ as $b\to 0$. That is, when the bias is small, the information transmitted is more precise.
\bullitem Equilibria with larger $n$ are more efficient: A direct calculation shows that more precision is beneficial to the players.
\bullitem Full revelation is not possible for $b>0$: The bias bounds away the informativeness.
\bullitem This result extends beyond quadratic utility
functions to strictly concave functions with a unique
maximum that increases with~$\theta$.
%\end{itemize}


\section{Cheap Talk}\label{se:CheapTalk}
% \subsection{Cheap-Talk Equilibria}
From now on (contrary to the model of Crawford and Sobel),
we assume all sets to be finite. In this section, we give a
characterization of equilibria of the cheap talk game. We
denote by $\sigma:\Theta\to\Delta(M)$ the behavior strategy
of player~1, which sends message $m$ with probability
$\sigma(m\mid\theta)$ upon observing state~$\theta$. The
behavior strategy of player~2 is denoted
$\tau:M\to\Delta(A)$ which chooses action $a$ upon observing
message~$m$.
%
% The unfolding of the game is summarized as follows.
%
%%\begin{itemize}
%\bullitem A state $\theta\in\Theta$ is drawn according to $p$ and told to player 1.
%\bullitem Player 1 sends a message $m\in M$ to player 2; his
%behavior strategy is denoted by $\sigma(m\mid\theta)$.
%\bullitem Player 2 chooses an action $a$; his behavior
%strategy is denoted by $\tau(a\mid m)$.
%\bullitem The players' payoffs are $u(\theta,a)$ and $v(\theta,a)$.
%%\end{itemize}

For any such game, there exists a babbling equilibrium where $\sigma$ does not depend on $\theta$ and $\tau$ does not depend on $m$. It is easy to see that if player 1 is silent, player 2 need not listen, and if player 2 does not listen, player 1 need not transmit any information.

To characterize all equilibria, let's look at equilibrium conditions on both sides. It is useful to re-interpret a strategy for player 1.

\paragraph{Splittings.}
Consider a set of states $\Theta$ and prior $p\in\Delta(\Theta)$.
The strategy of player 1 can be thought of a {\em statistical experiment} $\sigma:\Theta\to\Delta(M)$. Fixing such a $\sigma$, define the posterior probability of $\theta$ given $m$ and the total probability of $m$:
%\begin{itemize}
\[
p_m(\theta)=\mathbb{P}(\theta\mid
m)=\frac{\sigma(m\mid\theta)p(\theta)}{\sum_\zeta
\sigma(m\mid\zeta)p(\zeta)},
\]
and $\lambda_m=\mathbb{P}(m)=\sum_\zeta p(\zeta)\sigma(m\mid\zeta)$.
%\end{itemize}
Then
\[
p(\theta)=\sum_m\mathbb{P}(m)\mathbb{P}(\theta\mid
m)=\sum_m\lambda_mp_m(\theta).
\]
A strategy $\sigma$ induces a decomposition of the prior $p$ into a convex combination of posteriors. This is called {\em a splitting} of $p$.

Conversely, if $p=\sum_m\lambda_mp_m$ is a splitting, let
\[
\sigma(m\mid\theta)=\frac{\lambda_m p_m(\theta)}{p(\theta)}.
\]
Then $\mathbb{P}(\theta\mid m)=p_m(\theta)$ and $\mathbb{P}(m)=\lambda_m$ 
\citep{AumannMaschler1995}.
Therefore, any splitting is induced by some strategy of
player~1. We may thus identify splittings and strategies.

\paragraph{Equilibrium condition for player 2.} 
Given a belief $p$ over the states, let
\[
Y(p)=\{\alpha\in\Delta(A) \mid \sum_a \alpha(a)\sum_\theta
p(\theta)v(\theta,a)=\max_a \sum_\theta
p(\theta)v(\theta,a)\}.
\]
This is the set of mixed actions optimal for player~2 at
belief $p$, given a strategy $\sigma$ of player~1.
At a best reply, player~2, who receives the message $m$,
has belief $p_m$ and chooses $y_m\in Y(p_m)$.

\paragraph{Equilibrium condition for player 1.}
Suppose that player 2 chooses the mixed action
$y_m\in\Delta(A)$ after receiving message $m$.
The best reply of player 1 requires that
\[
\forall (\theta,m)\quad
\sigma(m\mid\theta)>0\quad\Rightarrow\quad
u(\theta,y_m)=\max_{m'}u(\theta,y_{m'}):=U_\theta~.
\]
Player~1 is indifferent between all messages (and thus
posteriors) induced with positive probability. Notice that
$\sigma(m\mid\theta)>0\Leftrightarrow p_m(\theta)>0$.
We get the following equilibrium condition:
For each $m$,

$u(\theta,y_m)\le \max_{m'}u(\theta,y_{m'})=U_\theta$, with equality if $p_m(\theta)>0$.

\paragraph{Equilibrium characterization.} 
Consider the set $\mathcal E$ of tuples
$(p,U,V)\in\Delta(\Theta)\times \reals^\Theta\times \reals$,
such that there exists $y\in\Delta (A)$ with:
\einr2em
%\begin{enumerate}
\rmitem{(i)} $U_\theta\ge u(\theta,y)$, with equality if $p(\theta)>0$,
\rmitem{(ii)} $y\in Y(p)$,
\rmitem{(iii)} $V=\sum_\theta p(\theta) v(\theta,y)$.
%\end{enumerate}

\noindent We call  $\mathcal E$ {\em the non-revealing equilibrium set}.

\noindent
Denote 
\[
\mathsf{conv}_U(\mathcal E)=\left\{\sum\lambda_m(p_m,U,V_m)
\mid \forall m\quad (p_m,U,V_m)\in\mathcal E\right\},
\]
which is the set of convex combinations of points in
$\mathcal E$ such that the $U$ component is fixed.
\begin{theorem}[\citealp{Forges1994}]
There is an equilibrium of $\Gamma(p)$ with payoff $(U,V)$ if and only if 
$(p,U,V)\in \mathsf{conv}_U(\mathcal E)$.
\end{theorem}
This simple result characterizes the set of tuples (belief, payoffs) such that the payoffs are sustained by an equilibrium of the game with those beliefs. The proof results directly from the discussion of best replies above.

The take from this result is that player 1 can use a splitting to obtain a convex combination of beliefs and payoffs, subject to player 2 choosing optimal actions and player 1's indifference conditions.

Remark that usual  refinements such as sequential equilibria do not restrict the set of equilibrium outcomes since each equilibrium outcome can be obtained  by strategies which assign positive probability to each message.

% \paragraph{Examples.}

\begin{example}[Full revelation]
This game has a fully revealing equilibrium:
\[
\begin{array}{ccc}
& a_1 & a_2\\
\cline{1-3}
\theta_1&(1,1)&(0,0)\\
\cline{1-3}
 & & \\
 \cline{1-3}

\theta_2&(0,0)&(3,3)\\
\cline{1-3}
\end{array} 
\]
\end{example} 

\begin{example}[No revelation]
The only equilibrium of this game is non-revealing:
\[
\begin{array}{cccccc}
& a_1 & a_2& a_3& &\\
\cline{1-4}
\theta_1&(3,-4)&(2,-1)& (1,0)& &p\\
\cline{1-4}
 & & & & &\\
 \cline{1-4}
\theta_2&(3,0)&(2,-1)&(1,-4)& &1-p\\
\cline{1-4}
\end{array}
\]
Here,
\[
Y(p)=\begin{cases}
\{a_1\} \text{ if } p<\frac14\\
\{a_2\} \text{ if } \frac14<p<\frac34\\
\{a_3\} \text{ if } p>\frac34\,.\\
\end{cases}
\]
It easy to see that it is not possible to split $p$ and keep player 1 indifferent.
\end{example}

\begin{example}[Partial revelation]
This game has a partially revealing equilibrium:
\[
\begin{array}{cccccccc}
& a_1 & a_2 & a_3 & a_4 & a_5& & \\
\cline{1-6}
\theta_1&(1,10)&(3,8)&(0,5)&(3,0)&(1,-8)& &p\\
\cline{1-6}
 & & & & & & &\\
 \cline{1-6}
\theta_2&(1,-8)&(3,0)&(0,5)&(3,8)&(1,10)& &1-p\\
\cline{1-6}
\end{array}
\]
Namely, 
\[
Y(p)=\begin{cases}
\{a_1\} \text{ if } p>\frac45\\
\{a_2\} \text{ if } \frac45>p>\frac58\\
\{a_3\} \text{ if } \frac58>p>\frac38\\
\{a_4\} \text{ if } \frac38>p>\frac15\\
\{a_5\} \text{ if } \frac15>p\,.\\
\end{cases}
\]
Hence, for $p=\frac14$, player~2 plays $a_4$ and player~1's
payoffs in either state are $3,3$.
For $p=\frac34$, player 2 plays $a_2$ and player~1's payoffs are $3,3$.
Player~1 can induce those beliefs by the splitting
\[
\frac12=(1/2)\frac14+(1/2)\frac34
\]
or equivalently by the strategy $\sigma(m_1\mid \theta_1)=\frac34=\sigma(m_2\mid \theta_2)$. 
This equilibrium payoff dominates the non-revealing and the fully revealing equilibrium.
\end{example}

% \subsection{Persuasion}
\section{Persuasion}
Some variations of the model are called \textit{persuasion} instead
of cheap talk, for instance, models of certifiable
communication: The message sets depend on the state, and
thus contain some {\em hard} evidence (see
\citealp{ForgesKoessler2005,ForgesKoessler2008}).

A popular variation is called {\em Bayesian Persuasion}
\citep{KamenicaGentzkow2011}.
In this model, the sender is able to commit to a strategy
before being informed of the state, and this strategy is
announced to the receiver.
A way to interpret this assumption is to think that the strategic decision of the sender is to choose the precision of the statistical experiment that the receiver will receive messages from. This approach is also called {\em information design}, by analogy with mechanism design where an agent chooses the game to be played. Instead of considering an exogenous information structure, there is an agent (the sender) who chooses the information structure strategically.

Let us solve the persuasion game starting from the same model as in the previous section. As before, the receiver with belief $p$ will choose a mixed action in
\[
Y(p)=\Bigl\{\alpha\in\Delta(A) \Bigm| \sum_a \alpha(a)\sum_\theta
p(\theta)v(\theta,a)=\max_a \sum_\theta
p(\theta)v(\theta,a)\Bigr\}.
\]
An equilibrium strategy of player 2 is thus a selection of this correspondence $\gamma(p)\in Y(p)$ for all beliefs $p$.
A strategy of the sender is still $\sigma:\Theta\to\Delta(M)$, also represented by a splitting
\[
p=\sum_m\lambda_mp_m.
\]
The best reply of player 1 is thus given by the following program:
\begin{equation}
\label{ugamma}
\sup\Bigl\{\sum_m\lambda_m\sum_\theta p_m(\theta)u(\theta,
\gamma(p_m)) \Bigm| p=\sum_m\lambda_mp_m\Bigr\}\,.
\end{equation}

\begin{proposition}[\citealp{KamenicaGentzkow2011}]
Let
\[
U_\gamma(p)=\sum_\theta p(\theta)u(\theta, \gamma(p)).
\]
An equilibrium payoff of player 1 is $\mathsf{cav} U_\gamma(p)$, the smallest concave function pointwise greater than $U_\gamma$ on $\Delta(\Theta)$, evaluated at $p$.
\end{proposition}

\paragraph{Remarks.}
Observe that, depending on the selection $\gamma(p)$, the
function $U_\gamma(p)$ may be discontinuous, and not even
upper-semi continuous.
The problem (\ref{ugamma}) may not have an optimal solution,
and only $\eps$-solutions yield $\eps$-equilibria of the
game.
Yet, the game admits an equilibrium: It
suffices to choose $\gamma(p)$ such that $U_\gamma(p)$ is
upper semi continuous.
The following choice of selection works:
\[
\gamma^*(p)\in\arg\max\Bigl\{\sum_a \alpha(a)\sum_\theta
p(\theta)u(\theta,a) \Bigm| \alpha\in Y(p)\Bigr\}.
\]
That is, when player 2 is indifferent, he chooses among the
optimal actions one which is preferred by player 1.
If we assume that the payoff function of player~2
satisfies the following regularity condition:
\[
\forall a\quad \exists p\quad  A(p)=\{a\},
\]
then  $\mathsf{cav\,} U_\gamma(p)$ does not depend on the
selection $\gamma(p)\in Y(p)$ and is the value of all limits
of $\eps$-equilibrium payoffs as $\eps\to 0$. To see this, notice that under the regularity condition, $A(p)$ is a singleton and thus $Y(p)=\{\gamma^*(p)\}$, except on a set of empty interior.

\section{Long Cheap Talk}
In this section, we study whether other communication
formats can lead to other equilibrium outcomes.
In the regular cheap talk model, player 1 sends a single
message.
Suppose that multiple rounds of messages are performed
before player~2 chooses an action.
Does this change equilibrium outcomes? If only player 1
sends messages, then up to
choosing a larger message space, we can assume that all
communication occurs in one round. 

In a \textit{long cheap talk model}, at every stage
$t=1,\dots, T,\dots$, both players send a cheap talk message
to the other (simultaneously).
Then, player~2 chooses an action (at a terminal stage $t+1$).
Let $\Gamma_T(p)$ be the cheap talk game with $T$ rounds and
prior distribution $p$.

\paragraph{Jointly controlled lotteries.} 
A jointly controlled lottery
\citep{AumannMaschler1995}
over a finite set $E$ is a procedure by which two players
choose a point in~$E$ with respect to some given probability
distribution, with the property that no player can change
the distribution by a unilateral deviation.

This is based on the following simple lemma. 

\begin{lemma} Let $X, Y$ be independent random variables in $\{1,\dots, N\}$ and let 
$Z=X+Y \mathsf{mod\,} N$. If $X$ is uniformly distributed, then so is $Z$.
\end{lemma}
Thus, to induce a jointly controlled lottery over
$\{1,\dots, N\}$ with the uniform distribution, it suffices
that each player draws a uniformly distributed random
variable and then add them modulo $N$.
This has a direct consequence:
\begin{corollary} Let $E$ be a finite set and $\mu\in\Delta(E)$ such that for each $e\in E$, there exists an integer $N_e$ such that $\mu(e)=\frac{N_e}{N}$. Then, there is a jointly controlled lottery over $E$ with distribution $\mu$.
\end{corollary}

Consider a two-player game $G=(A,B, u(a,b), v(a,b))$ and let $M$
be a finite message space.
Denote by $E(G)$ the set of Nash equilibrium payoffs of $G$.
Consider the following cheap talk extension of the game denoted $G^*_M$:

%\begin{itemize}
\bullitem First, players select messages simultaneously. Messages are publicly observed.
\bullitem Then, $G$ is played.
%\end{itemize}

\begin{lemma}
\[
\mathsf{conv}^M E(G)\subseteq E(G^*_M)\subseteq
\mathsf{conv} E(G),
\]
where $\mathsf{conv}^M$ denotes convex combinations with coefficients that are fractional in $|M|$.
\end{lemma}

The players simply use a jointly controlled lottery to
select a Nash equilibrium of $G$.
This principle can be applied to a sender-receiver game.
\begin{example}
In the game
\[
\begin{array}{cccccccc}
& a_1 & a_2 & a_3 & a_4 & a_5& & \\
\cline{1-6}
\theta_1&(1,10)&(3,8)&(0,5)&(3,0)&(1,-8)& &p\\
\cline{1-6}
 & & & & & & &\\
 \cline{1-6}
\theta_2&(1,-8)&(3,0)&(0,5)&(3,8)&(1,10)& &1-p~,\\
\cline{1-6}
\end{array}
\]
the payoff vector $\frac12((3,3),6)+\frac12((1,1),10)$ can be obtained by a two-step communication procedure. 
%\begin{itemize}
\bullitem In a first step, the two player generate a jointly
%controlled lottery $\frac12-\frac12$ over $\{\textit{Heads,
controlled uniform lottery over $\{\textit{Heads, Tails}\}$.
\bullitem In a second step, if \textit{Heads} occurs, then
they play the fully revealing equilibrium with payoff $((1,1), 10)$.
If \textit{Tails} occurs, then they play the partially
revealing equilibrium where player~1 performs the splitting
\[
\frac12=(1/2)\frac14+(1/2)\frac34.
\]
%\end{itemize}
\end{example}

\paragraph{Di-convexity.}
The example above shows two phenomena that may occur within
an equilibrium.
Player~1, using a splitting, can convexify the equilibrium
correspondence leaving his payoff fixed.
Both players, using a jointly controlled lottery, can
convexify the equilibrium payoffs without revealing
information, thus leaving the belief fixed.
Alternating such operations leads to the notions of
di-convex sets and di-martingale, due to
\citet{AumannHart2003}.

\begin{definition}
 %\begin{itemize}
A set $E\subseteq
\Delta(\Theta)\times\reals^\Theta\times\reals$ is
\textit{di-convex} 
if for all $p,U$, the sections $\{(U,V) \mid (p,U,V)\in E\}$
and $\{(p,V) \mid (p,U,V)\in E\}$ are convex.
The set $\dico(E)$ is the smallest di-convex superset of $E$.
%\end{itemize}
\end{definition}

\begin{definition}
%\begin{itemize}
A \textit{di-martingale}
is a martingale $(p_n,U_n,V_n)_n$ such that at each step, $p_{n+1}=p_n$ or $U_{n+1}=U_n$.
The set $\dispan(E)$ is the set of starting points of di-martingales with limit in~$E$.
%\end{itemize}
\end{definition}

\begin{theorem}[\citealp{Hart1985,Forges1994,AumannHart2003}]
\leavevmode\kern7em
%\begin{itemize}
% \abs{$\bullet$}
\bullitem
The payoff vector $(U,V)$ is an equilibrium payoff of the
game $\Gamma_T(p)$ for some $T$ if and only if
$(p,U,V)\in\dico(\mathcal E)$,
\bullitem
% \abs{$\bullet$}
the payoff vector $(U,V)$ is an equilibrium payoff of the
game $\Gamma_\infty(p)$ if and only if
$(p,U,V)\in\dispan(\mathcal E)$,
%\end{itemize}

 where $\mathcal E$ is the non-revealing equilibrium set defined in Section \ref{se:CheapTalk}.
\end{theorem}
The tools of di-martingales where first used by \citet{Hart1985}
for the characterization of equilibrium payoffs in repeated
games with incomplete information on one side.
\citet{Forges1994} specialized it to sender-receiver games
and \citet{AumannHart2003} studied long cheap talk models
using such tools. 

\noindent Equilibria are constructed by alternating jointly controlled lotteries and splitting.
\citet{Forges1990} gave the following example where the sets
$\dico(\mathcal E)$ and $\dispan(\mathcal E)$ differ:
\[
\begin{array}{cccccccc}
 & a_1 & a_2 & a_3 & a_4 & a_5 & \\ \cline{2-6}
 \theta_1 & (6,10) & (10,9) & (0,7) & (4,4) & (3,0) && p \\ \cline{2-6}
 & & & & & & \\ \cline{2-6}
 \theta_2 & (3,0) & (4,4) & (0,7) & (10,9) & (6,10) && 1-p \\ \cline{2-6}
 \\
\end{array}
\]
Figure \ref{fig:fourfrogs} represents the projection of the
set $\mathcal E$ on beliefs and payoffs of player~1,
intersected with the plane $u(\theta_1,\cdot)+
u(\theta_2,\cdot)=14$. The gray set corresponds to
$\dispan(\mathcal E)$, the set of starting points of
di-martingales which converge to a limit in~$\mathcal E$.
The reader is referred to \citet{Forges1990} for more
details.

\begin{figure}[hbt]%\label{fig:fourfrogs}
\begin{center}
\begin{tikzpicture}
\draw[thick,->] (0,0) -- (5,0)node[anchor=north west] {$p$};
\draw[thick,->]  (0,0) -- (0,4)node[anchor=south east] {$u$};
\draw[](3,1)--(4,1);
\draw[](1,3)--(2,3);
\draw[thick](2.5,2)node{$\bullet$};
\draw[thick](0,1.5)node{$\bullet$};
\draw[thick](5,2.5)node{$\bullet$};
\fill[black!20!white] (1,1.45) rectangle (4,2.55);
\fill[black!20!white] (1,1.5) rectangle (2,3);
\fill[black!20!white] (3,1) rectangle (4,2.55);
\fill[black!20!white] (0.05,1.45) rectangle (1,1.55);
\fill[black!20!white] (4,2.45) rectangle (4.95,2.55);
\fill[black!90!white] (1,3) rectangle (2,3.1);
\fill[black!90!white] (3,0.9) rectangle (4,1);
\end{tikzpicture}
\end{center}
\caption{Unbounded di-martingale}\label{fig:fourfrogs}
\end{figure}

In this example, the set $\mathcal E$ is
di-convex. Therefore, the cheap talk game with $T$ stages
has the same equilibrium payoffs as the game with $T=1$. In
order to get other equilibrium payoffs, the number of
communication steps has to be unbounded. This is an
adaptation of the four-frogs example of \citet{AumannHart1986}. 

\section{Mediated Talk}
Another way to organize communication is to use a mediator,
that is a dis-interested third party, who serves as an
intermediary between the sender and the receiver. The
mediator can use any communication strategy to which he
commits and which is known by both players. The resulting
equilibrium outcomes are called communication equilibria and
were defined by \citet{Myerson1986} and \citet{Forges1986}.

\paragraph{Communication equilibria.}
Consider the game given by $p\in\Delta(\Theta)$, $u(\theta,a)$, $v(\theta,a)$.
\begin{definition}
A \textit{communication equilibrium} is given by
%\begin{itemize}
a set of messages $M$, a~set of recommendations $R$,
a mapping $\mu:M\to\Delta(R)$,
and an equilibrium $(\sigma,\tau)$ of the game where:
%\begin{itemize} \parskip0pt
%\item[\raise.3ex\hbox{\scriptsize$\circ$}]
\bullitem
Player 1 knowing $\theta$ chooses a message $m$.
\bullitem
A recommendation is drawn with probability $\mu(r | m)$ and sent to player 2.
\bullitem
Player 2 chooses an action.
%\end{itemize}
\end{definition}
A communication equilibrium is a tuple $(\mu,\sigma,\tau)$
where $\mu$ is a mechanism and $(\sigma,\tau)$ is the
associated equilibrium.
If $|M|=1$, then $\mu$ simply transmits a constant message
$\mu(m\mid m)$, and we get a Nash equilibrium.
For a communication equilibrium $(\mu,\sigma,\tau)$, define
\[
% \mu^*(a\mid\theta)=\sum_{m,\,r}\tau(a\mid r)~\mu(r\mid m)~\sigma(m\mid\theta).
\mu^*(a\mid\theta)=\sum_{m,\,r}\tau(a|r)\,\mu(r|m)\,\sigma(m|\theta).
\]
Then, denoting by $\Id$ the identity function, it is easy to
see that $(\mu^*,\Id,\Id)$ is a communication equilibrium
inducing the same joint distribution over states and
actions. The mediator commits to playing the strategies instead of the players.
Thus, player 1 can announce the true state, and player 2
plays the action recommended.
This yields the following.

\begin{lemma}[Revelation principle]
Any communication equilibrium outcome can be obtained by a {\em canonical} communication equilibrium where $\mu:\Theta\to\Delta(A)$, $\sigma(\theta)=\theta$, $\tau(a)=a$.
\end{lemma}

We state the equilibrium conditions for this case.
Player~1 has an incentive to announce the true state, and
player~2 has an incentive to play the recommended actions.
\begin{lemma}
$\mu:\Theta\to\Delta(A)$ is a communication equilibrium if and only if:
%\begin{itemize}
% $\forall \theta,\theta'$, $\sum_a\mu(a|\theta)u(\theta,a)\ge \sum_a\mu(a|\theta')u(\theta,a),$
% $\forall a,a'$, $\sum_\theta p(\theta)\mu(a|\theta)v(\theta,a)\ge \sum_\theta p(\theta)\mu(a|\theta)v(\theta,a').$
%\end{itemize}
\[
\begin{array}{llcl}
\forall \theta,\theta'~~&
\sum_a\mu(a|\theta)\,u(\theta,a)&\ge&
\sum_a\mu(a|\theta')\,u(\theta,a),\\
\forall a,a'& 
\sum_\theta p(\theta)\,\mu(a|\theta)\,v(\theta,a)&\ge&
\sum_\theta p(\theta)\,\mu(a|\theta)\,v(\theta,a').\\
\end{array}
\]
\end{lemma}
A direct consequence is that the set of communication equilibria is a convex set which contains the Nash equilibria of the cheap talk game. A less obvious observation is that it also contains the outcomes of long cheap talk. This follows from the revelation principle: The mediator can construct a mechanism which replicates a (long) communication protocol. Still, player 1 has an incentive to announce the true state to the mediator, and player 2 plays the recommended action.

\begin{example}[Mediated talk dominates cheap talk]
\label{ex:med}
In the game
\[
\begin{array}{cccccc}
& a_1 & a_2& a_3& &\\
\cline{1-4}
\theta_1&(3,3)&(1,2)& (0,0)& &\frac12\\
\cline{1-4}
 & & & & &\\
 \cline{1-4} 
\theta_2&(2,0)&(3,2)&(1,3)& &\frac12\\
\cline{1-4}
\end{array}
\]
the only equilibrium with, possibly long, cheap talk is non-revealing.
This holds because it is not possible to split the prior and
keeping player~1's payoff constant.
The equilibrium payoffs are $\bigl((1,3), 2\bigr)$.

However, the mechanism $\mu$ such that
$\mu(a_1|\theta_1)=\mu(a_2|\theta_1)=\frac12$ and $\mu(a_2|\theta_2)=1$ is a
communication equilibrium with payoffs $\bigl((2,3), 2.25\bigr)$.
\end{example}

\begin{remark}\rm The persuasion model can be thought of a special kind of mediated talk where the role of the mediator is embodied by player 1. It is easy to see that the persuasion equilibrium payoff $\mathsf {cav\,}U_{\gamma^*}(p)$ is the maximum of the expected payoff of player 1 
\[
\max_\mu\sum_{\theta,a}
p(\theta)\,\mu(a|\theta)\,u(\theta,a),
\]
subject to the incentive constraints of player 2
\[
\forall a,a'\quad\sum_\theta
p(\theta)\,\mu(a|\theta)\,v(\theta,a)\ge \sum_\theta
p(\theta)\,\mu(a|\theta)\,v(\theta,a').
\]
If player 1 has the power to commit to a mechanism $\mu$, we
can separate his decisions as follows.
First, the strategic player 1 chooses $\mu$, which is
publicly announced.
Once $\mu$ is fixed, player 1 is replaced by a non-strategic
agent, or a player with constant payoffs, who reports the
state to the mechanism.
The incentive constraints for this non-strategic agent are
trivial; only those of player~2 remain.
The strategic player~1 chooses the mechanism that maximizes
his expected payoff.
\end{remark} 

\section{Dynamic Sender-Receiver Problems}
In this section we consider repeated sender-receiver models. In the long-cheap talk model of previous sections, a state is drawn once and for all and after several communication rounds, a single action is taken. In a repeated sender-receiver game, a state is drawn and an action is chosen in every period and payoffs are averaged over time. We study the efficiency and informativeness of equilibria, in comparison with the static games.

\paragraph{Repeated sender receiver game.}
The primitive data of the game are $p\in\Delta(\Theta)$, $u(\theta,a)$, $v(\theta,a)$ and a discount factor $\delta\in[0,1)$.
%\begin{itemize}
\bullitem A sequence of states $(\theta_1,\dots, \theta_t,\dots)$ is drawn i.i.d. from $p$.
\bullitem
At each stage $t$, player 1 is informed of
$\theta_t$, sends a message $m_t=\hat\theta_t$ to player 2
who chooses an action $a_t$\,. Messages and actions are publicly observed.
\bullitem
The average payoff is $(1-\delta)\sum_t\delta^{t-1}\,u_i(\theta_t,a_t)$.
%\end{itemize}

\paragraph{Individual rationality.}
In any equilibrium of the repeated game, a payoff vector
$(x,y)$ should be individually rational. Define
\[
\underline u(p)=\min_a u(p,a)=\min_a\sum_\theta
p(\theta)\,u(\theta,a)
\qquad\hbox{and}\qquad
\underline v(p)=\max_a v(p,a)\,.
\]
\begin{lemma} If $(x,y)$ is an equilibrium payoff of the repeated game, then 
$x\ge \underline u(p)$ and $y\ge \underline v(p)$.
\end{lemma}

This holds because on one hand, player~2 can punish player 1 by
choosing the worst action and on the other hand, player 1
can punish player 2 by revealing no information.

\paragraph{Incentive compatibility.}
With the intuition of the revelation principle, we assume
that the strategy of player~1 is to announce the true state
in each period and ask whether this can be an equilibrium
strategy.
In a one-shot cheap talk game, player~2 has no means for
deciding if the announcement of player~1 is true.
In the dynamic game however, the sequence of states
announced by player 1 should be plausibly the realization of
an i.i.d.\ process. 

For instance, suppose that there are two possible states
$\Theta=\{0,1\}$ and that $p$ is uniformly distributed.
If player 1 consistently announces the other state
$\hat\theta_t=1-\theta_t$, then the sequence of
announcements is also i.i.d. uniformly distributed.
Or, if player~1 would in each period tell the truth with
probability $\alpha$ and lie with probability $1-\alpha$,
then the sequence of announcements is still i.i.d.\
uniformly distributed.
Such deviations from truth-telling are undetectable by player~2.
The next definition formalizes this idea.

Given $\mu:\Theta\to\Delta(A)$, let
\[
U(\mu)=\sum_\theta p(\theta)\sum_a \mu(a|\theta)u(\theta,a).
\]
For $Q:\Theta\to\Delta(\Theta)$ let
\[pQ(\theta')=\sum_\theta p(\theta)Q(\theta'| \theta)\]
and 
\[
Q\mu(a|\theta)=\sum_{\theta'}Q(\theta'|\theta)\mu(a| \theta').
\]

\begin{definition}
$\mu$ is \textit{incentive compatible} if 
\[
pQ=p \quad\Rightarrow\quad U(Q\mu)\le U(\mu).
\]
$\mu$ is \textit{strongly incentive compatible} if
\[
pQ=p\quad\text{and}\quad U(Q\mu)\ge U(\mu)\quad\Rightarrow\quad Q\mu=\mu.
\]
%\end{itemize}
\end{definition}
A transition probability $Q$ represents a one-shot strategy of player 1: how the reported state is drawn, conditional on the true state. If $pQ=Q$ and player 1 plays $Q$ each period, then $Q$ is statistically undetectable by player 2. It cannot be distinguished from truth-telling since the sequence of announced states is i.i.d. $p$. 

\begin{lemma} If $(x,y)$ is an equilibrium payoff of the
repeated game, then there exists an incentive compatible $\mu$ 
such that $x=U(\mu)$.
\end{lemma}

This is an instance of a more general equilibrium condition
in repeated games, that in equilibrium no player has a profitable
undetectable deviation; see \citet{Lehrer1990}.
Notice that the set of incentive compatible $\mu$'s is
convex. So given a strategy of player 1 and a history of
the game, one can define $\mu$ as the (discounted) average
of the strategies played by player 1 along the history. If
this $\mu$ is not incentive compatible, this means that
player 1 could deviate without affecting the distribution of
messages received by player 2. Thus, the distribution of
actions is also unaffected and the deviation is profitable.

Strong incentive compatibility requires more.
It says that if the deviation is undetectable ($pQ=p$), then
the joint distribution of states and actions is unaffected
as well ($Q\mu=\mu$).

\begin{theorem}[\citealp{RenaultSolanVieille2013, RenouTomala2015}]
If $\mu$ is strongly incentive compatible and
$V(\mu)>\underline v(p)$, then for all $\eps>0$, there
exists $\delta_\eps$ such that for all $\delta>\delta_\eps$
there exists a subgame-perfect equilibrium of the repeated
game such that 
\[
\max_{\theta,a}\mathbb{E}\Bigl|\sum_{t}(1-\delta)\delta^{t-1}1\{\theta_t=\theta,
a_t=a\}-p(\theta)\mu(a\mid\theta)\Bigr|\le\eps~.
\]
\end{theorem}
This result says that any payoff vector
$(x,y)=(U(\mu),V(\mu))$ that is individually rational and
where $\mu$ is strongly incentive compatible, is an
equilibrium payoff for a sufficiently large discount factor.
The result is actually stronger as it characterizes the
empirical distributions of states and action which is close
to $p\otimes\mu$. 
Also, the equilibrium can be shown to be subgame-perfect.
A similar result for (non subgame-perfect) Nash equilibrium
would obtain assuming only incentive compatibility
(similarly to \citealp{Lehrer1990}).

The idea of the construction is that player 1 should in each period announce the true state $\theta$ to player 2 who should play $a$ with probability $\mu(a|\theta)$. Players monitor each other as follows.
%\begin{itemize}
\bullitem Player 2 calculates the statistical distribution of reports of player 1 and tests it against the theoretical distribution. Player 1 is punished for a large number of periods if he fails the test. 
\bullitem Player 1 also tests, conditional on his announcements, that player 2 plays actions in the theoretical proportions and punishes otherwise. 
\bullitem Periodically, at the end of review periods, payoffs in the continuation game are adjusted upwards or downwards.
%\end{itemize}
The reader is referred to \citet{Lehrer1990} for the
construction of similar strategies based on statistical
testing. To ensure subgame-perfection, the adjustment of
continuation payoffs is such that players have an incentive
to pass the tests rather than failing them. Yet, the
construction is not explicit about how a player passes a
test. This is where strong incentive compatibility is
important. This ensures that, as long as the statistics are
correct, the realized payoff are close to the targeted ones.
See \citet{Gossner1995} for a similar argument.

This result extends to ergodic (i.e., recurrent aperiodic)
Markov chains, see \cite{RenaultSolanVieille2013},
\cite{EscobarToikka2013}, and \cite{RenouTomala2015}. 

\begin{example}
In the game of Example \ref{ex:med},
\[
\begin{array}{cccccc}
& a_1 & a_2& a_3& &\\
\cline{1-4}
\theta_1&(3,3)&(1,2)& (0,0)& &\frac12\\
\cline{1-4}
 & & & & &\\
 \cline{1-4}
\theta_2&(2,0)&(3,2)&(1,3)& &\frac12~,\\
\cline{1-4}
\end{array}
\]
we want to implement $\mu$ such that $\mu(a_1|\theta_1)=1$
and $\mu(a_3|\theta_2)=1$, which is individually rational.
If $(\frac12,\frac12)Q=(\frac12,\frac12)$, then $Q$ has to
be the $2\times 2$ bi-stochastic matrix with diagonal
element $1-\alpha$.
Then
\[
U(Q\mu)=\frac12[(1-\alpha)3+\alpha
0]+\frac12[(1-\alpha)1+\alpha2]=2-\alpha<2=U(\mu).
\]
Therefore $\mu$ is strongly incentive compatible and the
corresponding payoff can be obtained within an equilibrium
of the repeated game.
As shown in Example \ref{ex:med}, the corresponding payoff
cannot be obtained by mediated one-shot talk.
\end{example}

\bibliographystyle{Eig}
\bibliography{bib-eig}
\addcontentsline{toc}{section}{References} 
